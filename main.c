/* Name: main.c
 * Project: USBaspLoader
 * Author: Christian Starkjohann
 * Modyfication: Grzegorz Hetman - ghetman@gmail.com - 2015-06-21
 * Creation Date: 2007-12-08
 * Tabsize: 4
 * Copyright: (c) 2007 by OBJECTIVE DEVELOPMENT Software GmbH
 * License: GNU GPL v2 (see License.txt)
 * This Revision: $Id$
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <avr/boot.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <string.h>

#include "usbdrv/usbdrv.c"

#define HAVE_EEPROM_PAGED_ACCESS     (USB_CFG_CLOCK_KHZ != 15000)
#define HAVE_EEPROM_BYTE_ACCESS      1

/* Request constants used by USBasp */
#define USBASP_FUNC_CONNECT         1
#define USBASP_FUNC_DISCONNECT      2
#define USBASP_FUNC_TRANSMIT        3
#define USBASP_FUNC_READFLASH       4
#define USBASP_FUNC_ENABLEPROG      5
#define USBASP_FUNC_WRITEFLASH      6
#define USBASP_FUNC_READEEPROM      7
#define USBASP_FUNC_WRITEEEPROM     8
#define USBASP_FUNC_SETLONGADDRESS  9

// Compatibility between ATMega8 and ATMega88.
#ifndef MCUCSR
#   define MCUCSR   MCUSR
#endif

// ATMega*8 don't have GICR, use MCUCR instead
#ifndef GICR
#   define GICR MCUCR
#endif

#ifndef ulong
#   define ulong    unsigned long
#endif
#ifndef uint
#   define uint     unsigned int
#endif

#if (FLASHEND) > 0xffff /* we need long addressing */
#   define CURRENT_ADDRESS  currentAddress.l
#   define addr_t           ulong
#else
#   define CURRENT_ADDRESS  currentAddress.w[0]
#   define addr_t           uint
#endif

typedef union longConverter{
    addr_t  l;
    uint    w[sizeof(addr_t)/2];
    uchar   b[sizeof(addr_t)];
}longConverter_t;

static uchar            requestBootLoaderExit;
static longConverter_t  currentAddress; /* in bytes */
static uchar            bytesRemaining;
static uchar            isLastPage;
#if HAVE_EEPROM_PAGED_ACCESS
static uchar            currentRequest;
#else
static const uchar      currentRequest = 0;
#endif

static const uchar  signatureBytes[4] = {
#ifdef SIGNATURE_BYTES
    SIGNATURE_BYTES
#elif defined (__AVR_ATmega8__) || defined (__AVR_ATmega8HVA__)
    0x1e, 0x93, 0x07, 0
#elif defined (__AVR_ATmega48__) || defined (__AVR_ATmega48P__)
    0x1e, 0x92, 0x05, 0
#elif defined (__AVR_ATmega88__) || defined (__AVR_ATmega88P__)
    0x1e, 0x93, 0x0a, 0
#elif defined (__AVR_ATmega168__) || defined (__AVR_ATmega168P__)
    0x1e, 0x94, 0x06, 0
#elif defined (__AVR_ATmega328P__)
    0x1e, 0x95, 0x0f, 0
#else
#   error "Device signature is not known, please edit main.c!"
#endif
};

static void (*nullVector)(void) __attribute__((__noreturn__));

uchar usbFunctionSetup(uchar data[8])
{
    uchar len = 0;
    usbRequest_t *rq = (void *)data;
    static uchar replyBuffer[4];

    usbMsgPtr = (usbMsgPtr_t)replyBuffer;
    if(rq->bRequest == USBASP_FUNC_TRANSMIT){   /* emulate parts of ISP protocol */
        uchar rval = 0;
        usbWord_t address;
        address.bytes[1] = rq->wValue.bytes[1];
        address.bytes[0] = rq->wIndex.bytes[0];
        if(rq->wValue.bytes[0] == 0x30){        /* read signature */
            rval = rq->wIndex.bytes[0] & 3;
            rval = signatureBytes[rval];
        #if HAVE_EEPROM_BYTE_ACCESS
        }else if(rq->wValue.bytes[0] == 0xa0){  /* read EEPROM byte */
            rval = eeprom_read_byte((void *)address.word);
        }else if(rq->wValue.bytes[0] == 0xc0){  /* write EEPROM byte */
            eeprom_write_byte((void *)address.word, rq->wIndex.bytes[1]);
        #endif
        #if HAVE_CHIP_ERASE
        }else if(rq->wValue.bytes[0] == 0xac && rq->wValue.bytes[1] == 0x80){  /* chip erase */
            addr_t addr;
            for(addr = 0; addr < FLASHEND + 1 - 2048; addr += SPM_PAGESIZE) {
                /* wait and erase page */
                DBG1(0x33, 0, 0);
                #ifndef NO_FLASH_WRITE
                boot_spm_busy_wait();
                cli();
                boot_page_erase(addr);
                sei();
                #endif
            }
        #endif
        }else{
            /* ignore all others, return default value == 0 */
        }
        replyBuffer[3] = rval;
        len = 4;
    }else if(rq->bRequest == USBASP_FUNC_ENABLEPROG){
        /* replyBuffer[0] = 0; is never touched and thus always 0 which means success */
        len = 1;
    }else if(rq->bRequest >= USBASP_FUNC_READFLASH && rq->bRequest <= USBASP_FUNC_SETLONGADDRESS){
        currentAddress.w[0] = rq->wValue.word;
        if(rq->bRequest == USBASP_FUNC_SETLONGADDRESS){
        #if (FLASHEND) > 0xffff
            currentAddress.w[1] = rq->wIndex.word;
        #endif
        }else{
            bytesRemaining = rq->wLength.bytes[0];
            /* if(rq->bRequest == USBASP_FUNC_WRITEFLASH) only evaluated during writeFlash anyway */
            isLastPage = rq->wIndex.bytes[1] & 0x02;
            #if HAVE_EEPROM_PAGED_ACCESS
            currentRequest = rq->bRequest;
            #endif
            len = 0xff; /* hand over to usbFunctionRead() / usbFunctionWrite() */
        }
    }else if(rq->bRequest == USBASP_FUNC_DISCONNECT){
        requestBootLoaderExit = 1;      /* allow proper shutdown/close of connection */
    }else{
        /* ignore: USBASP_FUNC_CONNECT */
    }
    return len;
}

uchar usbFunctionWrite(uchar *data, uchar len)
{
    uchar isLast;

    DBG1(0x31, (void *)&currentAddress.l, 4);
    if(len > bytesRemaining)
        len = bytesRemaining;
    bytesRemaining -= len;
    isLast = bytesRemaining == 0;
    if(currentRequest >= USBASP_FUNC_READEEPROM){
        uchar i;
        for(i = 0; i < len; i++){
            eeprom_write_byte((void *)(currentAddress.w[0]++), *data++);
        }
    } else {
        uchar i;
        for(i = 0; i < len;){
            #if !HAVE_CHIP_ERASE
            if((currentAddress.w[0] & (SPM_PAGESIZE - 1)) == 0){    /* if page start: erase */
                DBG1(0x33, 0, 0);
                #ifndef NO_FLASH_WRITE
                cli();
                boot_page_erase(CURRENT_ADDRESS);   /* erase page */
                sei();
                boot_spm_busy_wait();               /* wait until page is erased */
                #endif
            }
            #endif
            i += 2;
            DBG1(0x32, 0, 0);
            cli();
            boot_page_fill(CURRENT_ADDRESS, *(short *)data);
            sei();
            CURRENT_ADDRESS += 2;
            data += 2;
            /* write page when we cross page boundary or we have the last partial page */
            if((currentAddress.w[0] & (SPM_PAGESIZE - 1)) == 0 || (isLast && i >= len && isLastPage)){
                DBG1(0x34, 0, 0);
                #ifndef NO_FLASH_WRITE
                cli();
                boot_page_write(CURRENT_ADDRESS - 2);
                sei();
                boot_spm_busy_wait();
                cli();
                boot_rww_enable();
                sei();
                #endif
            }
        }
        DBG1(0x35, (void *)&currentAddress.l, 4);
    }
    return isLast;
}

uchar usbFunctionRead(uchar *data, uchar len)
{
    uchar i;

    if(len > bytesRemaining)
        len = bytesRemaining;
    bytesRemaining -= len;
    for(i = 0; i < len; i++){
        if(currentRequest >= USBASP_FUNC_READEEPROM){
            *data = eeprom_read_byte((void *)currentAddress.w[0]);
        }else{
            *data = pgm_read_byte((void *)CURRENT_ADDRESS);
        }
        data++;
        CURRENT_ADDRESS++;
    }
    return len;
}

static usbMsgLen_t usbFunctionDescriptor(struct usbRequest *rq) {
  return 0;
}

static void initForUsbConnectivity(void)
{
    uchar   i = 0;

    usbInit();
    /* enforce USB re-enumerate: */
    usbDeviceDisconnect();  /* do this while interrupts are disabled */
    while(--i){         /* fake USB disconnect for > 250 ms */
        wdt_reset();
        _delay_ms(1);
    }
    usbDeviceConnect();
    sei();
}

// By using .noinit section we are able to keep value of this variable
// even after reset condition.
uint8_t reset_cycle __attribute__ ((section (".noinit")));

int __attribute__((noreturn)) main(void)
{
    uchar i = 0;
    // main app may have enabled watchdog
    wdt_disable();

    odDebugInit();
    DBG1(0x00, 0, 0);

    #ifndef NO_FLASH_WRITE
    GICR = (1 << IVCE);  // enable change of interrupt vectors
    GICR = (1 << IVSEL); // move interrupts to boot flash section
    #endif

    if(MCUCSR & (1 << EXTRF)){
       reset_cycle++;
    }
    // After pressing 3 times reset button we jump into bootloader mode.
    if (reset_cycle == 3){
        uchar j = 0;
        initForUsbConnectivity();
        for(;;){
            usbPoll();
            if(requestBootLoaderExit){
                reset_cycle = 0;
                if(--i == 0){
                    if(--j == 0)
                        break;
                }
            }
        } 
    }

    // When reset cycle is above 4 set it to default value.
    if (reset_cycle >= 4) {
        reset_cycle = 0;
    }
    
    usbDeviceDisconnect();  /* do this while interrupts are disabled */
    while(--i){         /* fake USB disconnect for > 250 ms */
        wdt_reset();
        _delay_ms(20);
    }

    DBG1(0x01, 0, 0);
    MCUCSR = 0;
    cli();
    USB_INTR_ENABLE = 0;
    USB_INTR_CFG = 0;    // also reset config bits
    GICR = (1 << IVCE);  // enable change of interrupt vectors
    GICR = (0 << IVSEL); // move interrupts to application flash section
    // We must go through a global function pointer variable instead of writing
    // ((void (*)(void))0)();
    // because the compiler optimizes a constant 0 to "rcall 0" which is not
    // handled correctly by the assembler.
    nullVector();
}

